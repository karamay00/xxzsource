├── build                       构建相关
├── dist                        构建完成后的文件目录
├── mock                        项目mock模拟数据
├── node_modules                依赖类库
├── plop-templates              模板文件
├── public                      静态资源
│   ├── favicon.ico             favicon图标
│   └── index.html              html模板
├── src                         开发目录
│   ├── api                     所有请求
│   ├── assets                  主题、字体、图片等静态资源
│   ├── components              全局公用组件
│   ├── directive               全局指令
│   ├── filters                 全局filter
│   ├── icons                   项目所有svg、icons
│   ├── lang                    国际化language
│   ├── layout                  全局layout
│   ├── router                  路由
│   ├── store                   全局store管理
│   ├── styles                  全局样式
│   ├── utils                   全局公用方法
│   ├── vendor                  公用vendor
│   ├── views                   views所有页面
│   │   ├── app                 应用
│   │   ├── authority           权限
│   │   ├── dashboard           主页
│   │   ├── data                统计
│   │   ├── error-page          错误页
│   │   ├── finance             财务
│   │   ├── goods               商品
│   │   ├── home                店铺
│   │   ├── login               登录
│   │   ├── order               订单
│   │   ├── profile             个人信息
│   │   ├── redirect            跳转
│   │   ├── setting             设置
│   │   └── user                会员
│   ├── App.vue                 入口页面
│   ├── main.js                 入口文件、加载组件、初始化等
│   ├── permission.js           权限管理
│   └── settings.js             配置文件
├── tests                       测试目录
├── .editorconfig               editor编辑器配置
├── .env.development            开发环境变量配置
├── .env.production             生产环境变量配置
├── .env.staging                测试环境变量配置
├── .eslintignore               eslint忽略文件
├── .eslintrc.js                eslint配置项
├── .gitignore                  git忽略文件
├── .travis.yml                 自动化CI配置
├── babel.config.js             babel-loader配置
├── jest.config.js              测试配置
├── jsconfig.json               js配置
├── LICENSE                     许可
├── package.json                package.json
├── package-lock.json           package-lock.json
├── plopfile.js                 自动创建项目文件
├── postcss.config.js           postcss配置
├── README.md                   使用手册
└── vue.config.js               vue-cli配置